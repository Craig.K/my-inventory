# My Inventory

![Inventory App Preview](resources/preview_main_screen.png)

This application is designed to help you track inventory and keep track of information such as weight, value, and location.  Determine what you can safely load into a vehicle or trailer, find out how much your coin or jewelry collection is worth, or find out where you left that Little League participation trophy that you worked so hard for and just can't remember "where it went."

## Features

- Manage multiple different inventories
- Search inventories by item, keyword, and location
- Save encrypted, password-protected inventories
- View the cumulative weight and value of inventory and containers including their contents

![Inventory Preview](resources/preview_inventory.png)

## Requirements

- A Windows, Linux, or Apple operating system
- A venv virtual environment for the application (see installation instructions below)
- Required packages listed in requirements.txt (see installation instructions below)
- Python 3.7+

## Installation

### Windows

1. Using the Command Prompt, navigate to the application's parent directory
1. Create a virtual environment named app_env : `python3 -m venv app_env`
1. Activate the virtual environment: `app_env\Scripts\activate`
1. Install requirements: `pip3 install -r requirements.txt`
1. Start the application by double-clicking "RUN.bat" in the application's parent directory
